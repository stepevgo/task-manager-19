package ru.t1.stepanishchev.tm.command.task;

import ru.t1.stepanishchev.tm.model.Task;
import ru.t1.stepanishchev.tm.util.TerminalUtil;

import java.util.List;

public final class TaskListByProjectIdCommand extends AbstractTaskCommand {

    private final String NAME = "task-show-by-project-id";

    private final String DESCRIPTION = "Show task list by project id.";

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = getTaskService().findAllByProjectId(projectId);
        renderTasks(tasks);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}